package com.wirecard;

import static akka.pattern.PatternsCS.ask;

import java.time.Duration;

import com.wirecard.domain.DateTime;

import akka.actor.ActorRef;
import akka.http.javadsl.server.AllDirectives;
import akka.http.javadsl.server.Route;

public class RestApi extends AllDirectives {

  private final ActorRef dateTime;
  
  public RestApi(ActorRef dateTime) {
    this.dateTime = dateTime;
  }

  public Route createRoute() {
    return route(
        
        path("hello", () -> 
          parameter("name", param -> 
            get(() -> 
              complete("Say hello to akka-http, " + param)))),
        
        
        
        path("time", () -> 
          get(() -> 
            onSuccess(
              ask(dateTime, DateTime.GET_DATE_TIME, Duration.ofSeconds(3L)), 
              result -> complete(result.toString()))))
        );
  }
}
