package com.wirecard.domain;

import java.time.Instant;

import akka.actor.AbstractActor;
import akka.actor.Props;
import akka.japi.pf.ReceiveBuilder;

public class DateTime extends AbstractActor{

  public static Props props () {
    return Props.create(DateTime.class, DateTime::new);
  }

  @Override
  public Receive createReceive() {
    return ReceiveBuilder.create()
      .match(String.class, GET_DATE_TIME::equals, msg -> sender().tell(Instant.now(), self()))
      .matchAny(msg -> System.out.println("actor received: " + msg.toString()))
      .build();
  }
  
  //protocol
  public static final String GET_DATE_TIME = "GetDateTime";
}
