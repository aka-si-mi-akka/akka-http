package com.wirecard;

import static akka.actor.ActorSystem.create;
import static akka.stream.ActorMaterializer.create;

import com.wirecard.domain.DateTime;

import akka.NotUsed;
import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.http.javadsl.ConnectHttp;
import akka.http.javadsl.Http;
import akka.http.javadsl.model.HttpRequest;
import akka.http.javadsl.model.HttpResponse;
import akka.stream.ActorMaterializer;
import akka.stream.javadsl.Flow;

public class MainHttp {

  public static void main(String[] args) throws Exception {
    ActorSystem system = create();

    final Http http = Http.get(system);
    final ActorMaterializer materializer = create(system);
    
    ActorRef dateTime = system.actorOf(Props.create(DateTime.class, DateTime::new));
    RestApi api = new RestApi(dateTime);

    final Flow<HttpRequest, HttpResponse, NotUsed> routeFlow = api.createRoute().flow(system, materializer);
    http.bindAndHandle(routeFlow, ConnectHttp.toHost("localhost", 8080), materializer);
    
    System.out.println("server is up");
  }
}
